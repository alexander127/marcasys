        <footer class="footer-page">
            <div class="container-fluid">
                <?php wp_nav_menu( [
                    'menu_class'      => 'footer-nav',
                    'theme_location'  => 'header_menu',
                    'container'       => 'false'
                ] ); ?>
            </div>
        </footer>

        <?php if ( isset($_GET['vendor']) ) {

            $vendor_id = $_GET['vendor'];

            $products = get_posts( array(
                'numberposts' => -1,
                'post_type'   => 'product',
                'author'      => $vendor_id
            ) );

            if(isset($_GET['category']) ) {

                $category_id = $_GET['category'];

                $products = get_posts( array(
                    'numberposts' => -1,
                    'post_type'   => 'product',
                    'author'      => $vendor_id,
                    'tax_query' => array(
                        array(
                            'taxonomy' => 'product_cat',
                            'field' => 'term_id',
                            'terms' => $category_id)
                    )
                ) );
            }

            $i = 0;

            foreach ($products as $product): $i++
            ?>

            <div style="display: none;" id="descriptionModal<?= $i ?>" class="description-modal">
                <h3><?= $product->post_title ?></h3>
                <?= $product->post_content ?>
            </div>

        <?php
            endforeach;
        } ?>

    </div><!-- / wrapper -->
    <span class="fader"></span>

    <?php wp_footer(); ?>
</body>
</html>
