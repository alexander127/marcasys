<?php
/*
 *  Author: Todd Motto | @toddmotto
 *  URL: html5blank.com | @html5blank
 *  Custom functions, support, custom post types and more.
 */

/*------------------------------------*\
	External Modules/Files
\*------------------------------------*/

// Load any external files you have here

/*------------------------------------*\
	Theme Support
\*------------------------------------*/

if (function_exists('add_theme_support'))
{
    // Add Menu Support
    add_theme_support('menus');

    add_action( 'after_setup_theme', function(){
        register_nav_menus( [
            'header_menu'  => 'Header menu',
            'footer_menu'  => 'Footer menu'
        ] );
    } );

    // Add Thumbnail Theme Support
    add_theme_support('post-thumbnails');
    add_image_size('large', 700, '', true); // Large Thumbnail
    add_image_size('medium', 250, '', true); // Medium Thumbnail
    add_image_size('small', 120, '', true); // Small Thumbnail
    add_image_size('product', 750, 750, true); // Product Thumbnail

    // Localisation Support
    load_theme_textdomain('marcasys', get_template_directory() . '/languages');
}

/*------------------------------------*\
	Functions
\*------------------------------------*/

/*------------------------------------*\
	Actions + Filters + ShortCodes
\*------------------------------------*/

// Add Actions

// Remove Actions

// Add Filters

// Remove Filters

// Shortcodes


/*------------------------------------*\
	ShortCode Functions
\*------------------------------------*/

?>
