<?php /* Template Name: Vendors Page Template */ get_header(); ?>

<?php
if( !empty($_GET['category']) ) {
    $category_id = $_GET['category'];
    $term = get_term_by( 'id', $category_id, 'product_cat');
    $name = $term->name;

    $vendors = get_field('vendors', 'product_cat_'.$category_id);
} else {
    $all_vendors = get_users( array('role' => 'wcfm_vendor') );
    $vendors = array();
    foreach ($all_vendors as $vendor) {
        $vendors[] = $vendor->ID;
    }
}
?>

<div class="page">
    <div class="products-area">
        <div class="container-fluid">
            <div class="products-area__header">
                <h1><?= $name ?></h1>
            </div>

            <?php if (have_posts()): while (have_posts()) : the_post(); ?>
                <?php if( !empty($vendors) ): ?>
                <div class="row">
                    <?php foreach ($vendors as $vendor): ?>
                        <?php
                        $store_info = get_user_meta( $vendor, 'wcfmmp_profile_settings' );
                        $store_description = $store_info[0]['shop_description'];
                        $store_name = $store_info[0]['store_name'];
                        $banner_id = $store_info[0]['banner'];
                        $attachment_url = wp_get_attachment_url($banner_id, 'full');
                        //$store_url = wcfmmp_get_store_url( $vendor );
                        $store_url = "/vendor-page";

                        /*
                        if( !empty($_GET['category']) ) {
                            //$store_url = $store_url."?category=".$category_id;
                            $store_url = $store_url."?vendor=".$vendor."&category=".$category_id;
                        }
                        */
                        $store_url = $store_url."?vendor=".$vendor;
                        ?>
                        <div class="col-xs-12 col-sm-6 col-md-4">
                            <a href="<?= $store_url ?>" class="product-block">
                                <div class="product-block__wrap">
                                    <div class="product-block__cnt">
                                        <div class="product-block__img bg-to-img" style="background-image: url(<?= $attachment_url ?>);">
                                            <?= wp_get_attachment_image( $banner_id, 'full' ); ?>
                                        </div>
                                    </div>
                                </div>
                                <h3 class="product-block__title"><?= $store_name ?></h3>
                                <?php if( !empty($store_description) ): ?>
                                    <div class="product-block__text">
                                        <?= $store_description ?>
                                    </div>
                                <?php endif; ?>
                            </a><!-- / product-block modal-open -->
                        </div>
                    <?php endforeach; ?>
                </div>
                <?php endif; ?>
            <?php endwhile; ?>
            <?php endif; ?>
        </div>
    </div><!-- / products -->
</div>

<?php get_footer(); ?>
