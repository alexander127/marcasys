<?php /* Template Name: Vendor Page Template */ get_header(); ?>
<div class="page">
    <div class="products-area">
        <div class="container-fluid">
            <div class="products-area__header">
                <?php if ( isset($_GET['vendor']) ) {
                    $vendor_id = $_GET['vendor'];
                    $store_info = get_user_meta( $vendor_id, 'wcfmmp_profile_settings' );
                    $store_name = $store_info[0]['store_name'];
                    $shop_description = $store_info[0]['shop_description'];
                    $youtube_link = $store_info[0]['social']['youtube'];

                    //convert link
                    $link_arr = explode("/", $youtube_link);
                    $youtube_link = "https://www.youtube.com/embed/".end($link_arr);

                    if ( isset($_GET['category']) ) {
                        $category_id = $_GET['category'];
                        $term = get_term_by( 'id', $category_id, 'product_cat');
                        $store_name = $store_name." - ".$term->name;
                    }
                    ?>
                    <h2><?= $store_name ?></h2>
                <?php } else { ?>
                    <h2>Vendor Not Found</h2>
                <?php } ?>
            </div>

            <?php if ( isset($_GET['vendor']) ) {
                $products = get_posts( array(
                    'numberposts' => -1,
                    'post_type'   => 'product',
                    'author'      => $vendor_id
                ) );

                if(isset($_GET['category']) ) {
                    $products = get_posts( array(
                        'numberposts' => -1,
                        'post_type'   => 'product',
                        'author'      => $vendor_id,
                        'tax_query' => array(
                            array(
                                'taxonomy' => 'product_cat',
                                'field' => 'term_id',
                                'terms' => $category_id)
                        )
                    ) );
                }

                $i = 0;
                ?>
                <div class="row">
                    <?php foreach ($products as $product): $i++ ?>
                    <div class="col-xs-12 col-sm-6 col-md-4">
                        <div class="product-block">
                            <a data-fancybox data-options='{"src": "#descriptionModal<?= $i ?>", "touch": false}' href="javascript:;" class="product-block__wrap">
                                <div class="product-block__cnt">
                                    <div class="product-block__img bg-to-img" style="background-image: url(<?php echo get_the_post_thumbnail_url( $product->ID, 'product'  ); ?>);">
                                        <?php echo get_the_post_thumbnail( $product->ID, 'product' ); ?>
                                    </div>
                                </div>
                            </a>
                            <h3 class="product-block__title"><?= $product->post_title ?></h3>
                            <a data-fancybox data-options='{"src": "#descriptionModal<?= $i ?>", "touch": false}' href="javascript:;" class="btn btn-default">Buy It</a>
                            <div class="product-block__text"><?= $product->post_excerpt ?></div>
                        </div><!-- / product-block modal-open -->
                    </div>
                    <?php endforeach; ?>
            </div>
            <?php } ?>
        </div>
    </div><!-- / products -->

    <?php if ( isset($_GET['vendor']) ): ?>

        <div class="products-area _pb">
            <div class="container-fluid">
                <div class="products-area__header">
                    <h2>Video</h2>
                </div>
                <div class="about-block__img">
                    <iframe class="about-block__video" src="<?= $youtube_link ?>" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                </div>
            </div>
        </div>

        <div class="products-area">
            <div class="container-fluid">
                <div class="products-area__header">
                    <h2>About</h2>
                </div>
                <?= $shop_description ?>
            </div>
        </div>

    <?php endif; ?>
</div>

<?php get_footer(); ?>
