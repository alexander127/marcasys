<?php get_header(); ?>

<?php $all_vendors = get_users( array('role' => 'wcfm_vendor') ); ?>
    <div class="page">
        <div class="container-fluid">
            <section class="products-area">
                <!--<div class="products-area__header">
                    <h2>Our Marketplace</h2>
                </div>-->

                <?php
                $categories = get_terms( ['taxonomy' => 'product_cat', 'hide_empty' => false] );
                if( !empty($categories) ):
                    ?>
                    <div class="categories-list row">
                        <?php foreach ($categories as $category): ?>
                            <div class="categories-list__item col-xs-6 col-sm-4">
                                <?php
                                $meta = get_term_meta( $category->term_id );
                                $thumb_id = $meta['thumbnail_id'][0];
                                $attachment_url = wp_get_attachment_url($thumb_id, 'full');
                                ?>
                                <a href="/vendor-list?category=<?= $category->term_id ?>" class="categories-list__block">
                                    <div class="categories-list__img bg-to-img" style="background-image: url(<?= $attachment_url ?>);">
                                        <?= wp_get_attachment_image( $thumb_id, 'full' ); ?>
                                    </div>
                                    <h3 class="categories-list__title"><?= $category->name ?></h3>
                                </a>
                            </div>
                        <?php endforeach; ?>
                    </div>
                <?php endif; ?>

                <?php if( !empty($all_vendors) ): ?>
                    <a href="/vendor-list" class="products-area__link hidden">Show All Vendors</a>
                <?php endif; ?>
            </section>

            <?php if( !empty($all_vendors) ): ?>
                <section class="products-area">
                    <div class="products-area__header">
                        <h2>Featured companies</h2>
                    </div>
                    <div class="row">
                        <?php
                        shuffle($all_vendors);
                        $all_vendors = array_slice($all_vendors, 0, 3);
                        foreach ($all_vendors as $vendor):
                            $store_info = get_user_meta( $vendor->ID, 'wcfmmp_profile_settings' );
                            $store_description = $store_info[0]['shop_description'];
                            $banner_id = $store_info[0]['banner'];
                            $attachment_url = wp_get_attachment_url($banner_id, 'full');
                            //$store_url = wcfmmp_get_store_url( $vendor->ID );
                            ?>
                            <div class="col-xs-12 col-sm-6 col-md-4">
                                <a href="/vendor-page?vendor=<?= $vendor->ID ?>" class="product-block">
                                    <div class="product-block__wrap">
                                        <div class="product-block__cnt">
                                            <div class="product-block__img bg-to-img" style="background-image: url(<?= $attachment_url ?>);">
                                                <?= wp_get_attachment_image( $banner_id, 'full' ); ?>
                                            </div>
                                        </div>
                                    </div>
                                    <h3 class="product-block__title"><?= $vendor->store_name ?></h3>
                                    <?php if( !empty($store_description) ): ?>
                                        <div class="product-block__text">
                                            <?= $store_description ?>
                                        </div>
                                    <?php endif; ?>
                                </a><!-- / product-block modal-open -->
                            </div>
                        <?php endforeach; ?>
                    </div>
                </section>
            <?php endif; ?>

        </div>
    </div>
<?php get_footer(); ?>